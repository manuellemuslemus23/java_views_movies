package com.manuellemus.viewmovies.model;

import java.util.ArrayList;

public class Chapter extends Movie{
    private int id;
    private int sessionNumber;

    public Chapter(String title, String genre, String creator, int duration, short year, int sessioonNumber) {
        super(title, genre, creator, duration, year);
        this.sessionNumber = sessioonNumber;
    }

    public int getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(int sessionNumber) {
        this.sessionNumber = sessionNumber;
    }
    //makeChaptherList
    @Override
    public String toString() {

        return  "\n---------------------------------" +
                "\nTitle: " + getTitle() +
                "\n---------------------------------" +
                "\n Genero: " + getGenre() +
                "\n Year: " + getYear() +
                "\n Creator: " + getCreator() +
                "\n Duration: " + getDuration() +
                "\n----------------------------------";
    }
    public static ArrayList<Chapter> makeChaptersList()
    {
        ArrayList<Chapter> chapters = new ArrayList<>();

        for (int i = 1; i<= 5; i++)
        {
            chapters.add(new Chapter("Episodio " + i ,"Action y drama "  + i,"Desconocido"  + i,120, (short)( 2010  + i), i));
        }
        return chapters;
    }
}
