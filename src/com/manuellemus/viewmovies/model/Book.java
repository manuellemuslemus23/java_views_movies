package com.manuellemus.viewmovies.model;

import java.util.Date;

public class Book extends Publication implements IVisualizable{
    private int id;
    private String isbn;
    private boolean readed;
    private int TimeReaded;

    public Book(String title, Date editionDate, String editorial) {
        super(title, editionDate, editorial);
    }

    public int getId() {
        return id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    public int getTimeReaded() {
        return TimeReaded;
    }

    public void setTimeReaded(int timeReaded) {
        TimeReaded = timeReaded;
    }
    //@Override
    public String toString() {

        String detailBook = "\n---------------------------------" +
                            "\nTitle: " + getTitle() +
                            "\n---------------------------------" +
                            "\n Editorial: " + getEditorial() +
                            "\n Edition Date: " + getEditionDate() +
                            "\n Authors: " +
                            "\n----------------------------------";
        for (int i = 0; i < getAuthors().length; i++ )
        {
            detailBook += "\t" + getAuthors()[i];
        }
        return detailBook;
    }

    @Override
    public Date startToSee(Date dateI) {
        return dateI;
    }

    @Override
    public void stopToSee(Date dateI, Date dateF) {
        if (dateF.getSeconds() > dateI.getSeconds()) {
            setTimeReaded(dateF.getSeconds() - dateI.getSeconds());
        }else{
            setTimeReaded(0);
        }
    }
}
