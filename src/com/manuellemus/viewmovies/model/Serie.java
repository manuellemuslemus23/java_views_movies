package com.manuellemus.viewmovies.model;

import java.util.ArrayList;

public class Serie extends Film{

    private int id;
    private int sessionQuantity;
    private ArrayList<Chapter> chapters;

    public Serie(String title, String genre, String creator, int duration, ArrayList<Chapter>  chapters,int sessionQuantity) {
        super(title, genre, creator, duration);
        this.sessionQuantity = sessionQuantity;
        this.chapters = chapters;
    }

    public int getId()
    {
        return id;
    }

    public ArrayList<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<Chapter> chapters) {
        this.chapters = chapters;
    }
    @Override
    public String toString() {

        return  "\n---------------------------------" +
                "\nTitle: " + getTitle() +
                "\n---------------------------------" +
                "\n Genero : " + getGenre() +
                "\n Year: " + getYear() +
                "\n Creator: " + getCreator() +
                "\n Duration: " + getDuration() +
                "\n----------------------------------";
    }
    public static ArrayList<Serie> makeSerieList()
    {
        ArrayList<Serie> series = new ArrayList<>();

        for (int i = 1; i<= 5; i++)
        {
            series.add(new Serie("BlackList " + i ,"Action vol "  + i,"Discovery chanel"  + i,120, Chapter.makeChaptersList(), i));
        }
        return series;
    }
}
