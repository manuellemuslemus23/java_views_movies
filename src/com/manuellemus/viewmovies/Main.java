package com.manuellemus.viewmovies;

import com.manuellemus.viewmovies.model.Chapter;
import com.manuellemus.viewmovies.model.Movie;
import com.manuellemus.viewmovies.model.Serie;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner; //importando la clase scanner para leer datos

public class Main {
    public static void main (String[] args)
    {
        System.out.println();
        System.out.println("BIENVENIDO a VIEWMOVIES");
        System.out.println();
//        Movie movie = new Movie("Avengers","Accion","",120, (short) 2019);
//        //movie.showMovie();
//        System.out.println(movie);
        showMenu();

    }

    public static void showMenu(){
        int exit = 0;
        int opcion = 5;
        do {
            System.out.println("1. Series ");
            System.out.println("2. Movies ");
            System.out.println("3. Books");
            System.out.println("4. Magazines");
            System.out.println("5. Report");
            System.out.println("6. ReportToday");
            System.out.println("0. Exit");
        }while(exit != 0);

        System.out.println();
        System.out.println("Ingrese una opcion :");

        Scanner teclado = new Scanner(System.in);
        opcion = teclado.nextInt();

        switch (opcion){
            case 0:
                exit = 0;
                break;
            case 1:
                showSeries();
                break;
            case 2:
                showMovies();
                break;
            case 3:
                showBooks();
                break;
            case 4:
                showMagazines();
                break;
            case 5:
                makeReport();
                break;
            case 6:
                //Date date = new Date();
                makeReport(new Date());
                break;
            default:
                System.out.println("your selec a valid option");
                break;
        }
    }

    public static void showSeries()
    {
        int exit = 1;
        ArrayList<Serie> series = Serie.makeSerieList();

        do{
            System.out.println();
            System.out.println("/-----------------|");
            System.out.println("|      SERIES     |");
            System.out.println("|-----------------/");
            for (int i = 0; i < series.size(); i++)
            {
                System.out.println( "\n---------------------------------" +
                        "\nTitle: " + series.get(i).getTitle() +
                        "\n Visto: " + series.get(i).isViewed());
            }
            System.out.println("0. Regresar al Menu ");
            System.out.println();
            //Leer respuesta
            Scanner sc = new Scanner(System.in);
            int response = sc.nextInt();
            if (response == 0)
            {
                showMenu();
            }

            showChapters(series.get(response - 1).getChapters());

        }while (exit != 0);
    }

    public static void showChapters(ArrayList<Chapter> chapterOfSerieSelected)
    {
        int exit = 0;
        do {
            System.out.println();
            System.out.println("/-----------------|");
            System.out.println("|      CHAPTERS     |");
            System.out.println("|-----------------/");
            System.out.println();
            for (int i = 0; i < chapterOfSerieSelected.size(); i++)
            {
                System.out.println( "\n---------------------------------" +
                        "\nTitle: " + chapterOfSerieSelected.get(i).getTitle() +
                        "\n Visto: " + chapterOfSerieSelected.get(i).isViewed());
            }
            System.out.println("0. Regresar al Menu ");
            System.out.println();

            //Leer respuesta
            Scanner sc = new Scanner(System.in);
            int response = sc.nextInt();
            if (response == 0)
            {
                showSeries();
            }

            //obteniendo el momento en que incio a ver la pelicula
            Chapter chapterSelected = chapterOfSerieSelected.get(response - 1);
            chapterSelected.setViewed(true);
            Date dateI = chapterSelected.startToSee(new Date());

            for (int i = 1; i < 1000; i++)
            {
                System.out.println();
                System.out.println("......");
                System.out.println();
            }
            //termine de verla
            chapterSelected.stopToSee(dateI, new Date());
            System.out.println();
            System.out.println("Viste :" + chapterSelected.getTitle());
            System.out.println("Por: " + chapterSelected.getTimeViewed() + "milisegundos");

        }while(exit != 0);
    }
    public static void showMovies()
    {
        int exit = 1;
        ArrayList<Movie> movies = Movie.makeMoviesList();
        do{
            System.out.println();
            System.out.println("/-----------------|");
            System.out.println("|      MOVIES     |");
            System.out.println("|-----------------/");
            System.out.println();
            for (int i = 0; i < movies.size(); i++)
            {
                System.out.println( "\n---------------------------------" +
                                    "\nTitle: " + movies.get(i).getTitle() +
                                    "\n Visto: " + movies.get(i).isViewed());
            }
            System.out.println("0. Regresar al Menu ");
            System.out.println();

            //Leer respuesta
            Scanner sc = new Scanner(System.in);
            int response = sc.nextInt();
            if (response == 0)
            {
                showMenu();
            }
            //obteniendo el momento en que incio a ver la pelicula
            Movie movieSelected = movies.get(response - 1);
            movieSelected.setViewed(true);
            Date dateI = movieSelected.startToSee(new Date());

            for (int i = 1; i < 1000; i++)
            {
                System.out.println();
                System.out.println("......");
                System.out.println();
            }

            //obteniendo el tiempo cuando termidondena de ver la pelicula
            movieSelected.stopToSee(dateI, new Date());
            System.out.println();
            System.out.println("Viste :" + movieSelected.getTitle());
            System.out.println("Por: " + movieSelected.getTimeViewed() + "milisegundos");

        }while (exit != 0);
    }
    public static void showBooks()
    {
        int exit = 0;
        do{
            System.out.println();
            System.out.println("/-----------------|");
            System.out.println("|      BOOKS      |");
            System.out.println("|-----------------/");
        }while (exit != 0);
    }
    public static void showMagazines()
    {
        int exit = 0;
        do{
            System.out.println();
            System.out.println("/--------------------|");
            System.out.println("|      MAGAZINES     |");
            System.out.println("|--------------------/");
        }while (exit != 0);
    }
    public  static  void makeReport()
    {

    }
    public  static void makeReport(Date date)
    {

    }
}
